FROM mcr.microsoft.com/dotnet/core/sdk:3.1 as build-env

WORKDIR /app

COPY . ./

RUN dotnet restore ./BankLibrary/BankLibrary/BankLibrary.csproj
RUN dotnet restore ./BankApplication.csproj

RUN dotnet publish -c Release -o folder

ENTRYPOINT ["dotnet", "/app/folder/BankApplication.dll"]